console.log("Hello Wolrd!");

let firstName = 'Ian';
console.log("First Name: " + firstName);

let lastName = 'De Alday';
console.log("Last Name: " + lastName);

let age = 27
console.log("Age: " + age);

console.log("Hobbies:")

let hobbies = [ 'Traveling', 'Watching movies', 'Playing mobile and computer games', 'Adventures' ]
console.log(hobbies);

let workAddress = {
		houseNumber: '32',
		street: 'San Pablo', 
		city: 'East Blue',
		state: 'New World'
		
	}
	console.log("Work address:")
	console.log(workAddress)


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let agee = 40;
	console.log("My current age is: " + agee);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestfriendName = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriendName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);