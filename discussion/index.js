// console.log("Hello World!")

// COMMENTS in JavaScript
// - Two write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multi-line comments. 
// This is a single line comment. single line = ctrl + /
/*
	This is a multi-line comment.
	To write them, we add to asterisks inside of the forward slashes and wrote comments in between them
	multi line = ctrl + shift + /
*/


// Variable
/*
	- Variabless contain values that can be changed over the execution time of the program.
	- To declare a variable, we use the "let" keyword.

*/
// variable declaration
let productName = 'dekstop computer';
console.log(productName);
productName = 'cellphone'
console.log(productName)

// initialization
let productPrice = 500;
console.log(productPrice);
productPrice = 450;
console.log(productPrice)


// CONSTANTS
/*
	- Use constants for values that will not change.
*/

const deliveryFee = 30;
console.log(deliveryFee);


// DATA TYPES


// 1. STRINGS
/*
	- Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT"
	- Strings in JavaScript can be written using a single quote('') or double quote ("")
	- On the other programming languages, only the double quote can be used for creating strings.
*/


let country = 'Philppines';
let province = "Metro Manila" ;

// CONCATENATION
console.log(country + ", " + province);
// ", " comma and space is used for space


// 2. NUMBERS
/*
	- Include integers/whole numbers, decimal numbers, fraction, exponential notation. No need to use quotaion marks. because the js will declare it as text not a number
*/

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);

// exact PI value
console.log(Math.PI)

console.log ("John's grace last quarter is: " + grade);


// 3. BOOLEAN
/*
	- Boolean values are logical values
	- Boolean values contain either "true" or "false"
*/

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);
console.log(isMarried)
console.log(isGoodConduct)


// 4. Objects

	// Arrays
	/*
		- They are a special kind of data that stores multiple values.
		- They are special type of an object.
		- They can store different data types but is normally used to store similar data types.
	*/
	
	// Syntax:
		// let/cons arrayName = [ElementA, ElementB, ElementC...]
	let grades = [ 98.7, 92.1, 90.2, 94.6 ];
	console.log(grades);
	console.log(grades[3]);


	let userDetails = ["John", "Smith", 32, true];
	console.log(userDetails);

	// Objects Literals
	/*
		- Objects are another special kind of data type that mimics real workd objects/items
		- They are used to create complex data  that contains pieces of information that are relevant to each other.
		- Every indvidual piece of information if caled a property of the object

		Syntax:

			let/const objectName = {
				PropertyA: value,
				PropertyB: value
			}

	*/

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)